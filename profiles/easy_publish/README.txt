This is a customized Drupal publishing system that comes with one basic responsive theme and fourteen subthemes. This publishing system is good for any company, business or person who wants to publish content online. It's easy to install and ready to go after installation. The publishing system contains six content types which all were built to suit the needs of a publishing system. 


After Install:
1- Go to admin/structure/ds/list/extras and change Default Field Template from Drupal Default to Expert.(You must do that to apply most of the css).
2- Go to admin/config/media/file-system and add your private files.
3- Go to admin/config/people/captcha and set the contact_site_form, user_login and user_register_form and set the captcha to image(from module image captcha).
4- Go to admin/appearance and pick the theme color you like.
5- Go to node/add/page and create a page for your terms and conditions.
6- Go to admin/config/people/terms_of_use and insert your terms and conditions page.
7- Go to admin/structure/taxonomy and insert some terms for each vocabulary.  (The URL for each page is structured with terms).


To Activate Facebook and Twitter for users login.
1- Go to admin/config/services/twitter/settings and insert twitter consumer key and consumer secret.
2- Go to admin/config/services/twitter and add authenticated twitter account (This step is must if you want to post to twitter)
3- Go to admin/structure/fb/fb_app_create and insert facebook App ID and secret.
