; easy_publish make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.0"
projects[module_filter][subdir] = "contrib"

projects[ctools][version] = "1.9"
projects[ctools][subdir] = "contrib"

projects[comment_easy_reply][version] = "1.7"
projects[comment_easy_reply][subdir] = "contrib"

projects[date][version] = "2.9-rc1"
projects[date][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[ds][version] = "2.11"
projects[ds][subdir] = "contrib"

projects[fb][version] = "3.4"
projects[fb][subdir] = "contrib"

projects[fb][version] = "3.4"
projects[fb][subdir] = "contrib"

projects[features][version] = "2.6"
projects[features][subdir] = "contrib"

projects[uuid_features][version] = "1.0-alpha4"
projects[uuid_features][subdir] = "contrib"

projects[email][version] = "1.3"
projects[email][subdir] = "contrib"

projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = "contrib"

projects[field_slideshow][version] = "2.0-beta1"
projects[field_slideshow][subdir] = "contrib"

projects[filefield_paths][version] = "1.0-beta4"
projects[filefield_paths][subdir] = "contrib"

projects[link][version] = "1.3"
projects[link][subdir] = "contrib"

projects[kingtut_theme_base][version] = "7.x"
projects[kingtut_theme_base][subdir] = "contrib"

projects[flexslider][version] = "2.0-rc1"
projects[flexslider][subdir] = "contrib"

projects[flexslider_views_slideshow][version] = "2.x-dev"
projects[flexslider_views_slideshow][subdir] = "contrib"

projects[imagecache_token][version] = "1.0-rc1"
projects[imagecache_token][subdir] = "contrib"

projects[pathologic][version] = "3.0-beta2"
projects[pathologic][subdir] = "contrib"

projects[html5_tools][version] = "1.2"
projects[html5_tools][subdir] = "contrib"

projects[file_entity][version] = "2.0-beta2"
projects[file_entity][subdir] = "contrib"

projects[media][version] = "2.0-beta1"
projects[media][subdir] = "contrib"

projects[media_vimeo][version] = "2.1"
projects[media_vimeo][subdir] = "contrib"

projects[media_youtube][version] = "3.0"
projects[media_youtube][subdir] = "contrib"

projects[microdata][version] = "1.0-beta2"
projects[microdata][subdir] = "contrib"

projects[oauth][version] = "3.2"
projects[oauth][subdir] = "contrib"

projects[addtoany][version] = "4.9"
projects[addtoany][subdir] = "contrib"

projects[advanced_help][version] = "1.3"
projects[advanced_help][subdir] = "contrib"

projects[better_formats][version] = "1.0-beta1"
projects[better_formats][subdir] = "contrib"

projects[breakpoints][version] = "1.3"
projects[breakpoints][subdir] = "contrib"

projects[db_maintenance][version] = "1.2"
projects[db_maintenance][subdir] = "contrib"

projects[diff][version] = "3.2"
projects[diff][subdir] = "contrib"

projects[elements][version] = "1.4"
projects[elements][subdir] = "contrib"

projects[elysia_cron][version] = "2.1"
projects[elysia_cron][subdir] = "contrib"

projects[entity][version] = "1.6"
projects[entity][subdir] = "contrib"

projects[fitvids][version] = "1.17"
projects[fitvids][subdir] = "contrib"

projects[flippy][version] = "1.4"
projects[flippy][subdir] = "contrib"

projects[friendly_register][version] = "1.2"
projects[friendly_register][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[linkchecker][version] = "1.2"
projects[linkchecker][subdir] = "contrib"

projects[logintoboggan][version] = "1.5"
projects[logintoboggan][subdir] = "contrib"

projects[pathauto][version] = "1.2"
projects[pathauto][subdir] = "contrib"

projects[realname][version] = "1.2"
projects[realname][subdir] = "contrib"

projects[redirect][version] = "1.0-rc3"
projects[redirect][subdir] = "contrib"

projects[restws][version] = "2.4"
projects[restws][subdir] = "contrib"

projects[role_export][version] = "1.0"
projects[role_export][subdir] = "contrib"

projects[scheduler][version] = "1.3"
projects[scheduler][subdir] = "contrib"

projects[scroll_to_top][version] = "2.1"
projects[scroll_to_top][subdir] = "contrib"

projects[site_map][version] = "1.3"
projects[site_map][subdir] = "contrib"

projects[site_verify][version] = "1.1"
projects[site_verify][subdir] = "contrib"

projects[smart_paging][version] = "2.11"
projects[smart_paging][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[terms_of_use][version] = "1.2"
projects[terms_of_use][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[transliteration][version] = "3.2"
projects[transliteration][subdir] = "contrib"

projects[twitter][version] = "5.10"
projects[twitter][subdir] = "contrib"

projects[entitycache][version] = "1.2"
projects[entitycache][subdir] = "contrib"

projects[expire][version] = "2.0-rc4"
projects[expire][subdir] = "contrib"

projects[view_unpublished][version] = "1.2"
projects[view_unpublished][subdir] = "contrib"

projects[picture][version] = "2.12"
projects[picture][subdir] = "contrib"

projects[print][version] = "2.0"
projects[print][subdir] = "contrib"

projects[rules][version] = "2.9"
projects[rules][subdir] = "contrib"

projects[metatag][version] = "1.7"
projects[metatag][subdir] = "contrib"

projects[search_api][version] = "1.16"
projects[search_api][subdir] = "contrib"

projects[search_api_page][version] = "1.2"
projects[search_api_page][subdir] = "contrib"

projects[search_api_ranges][version] = "1.5"
projects[search_api_ranges][subdir] = "contrib"

projects[facetapi][version] = "1.5"
projects[facetapi][subdir] = "contrib"

projects[captcha][version] = "1.3"
projects[captcha][subdir] = "contrib"

projects[google_analytics][version] = "2.1"
projects[google_analytics][subdir] = "contrib"

projects[taxonomy_menu][version] = "2.0-alpha2"
projects[taxonomy_menu][subdir] = "contrib"

projects[uuid][version] = "1.0-alpha6"
projects[uuid][subdir] = "contrib"

projects[responsive_menus][version] = "1.5"
projects[responsive_menus][subdir] = "contrib"

projects[wysiwyg][version] = "2.x-dev"
projects[wysiwyg][subdir] = "contrib"

projects[better_exposed_filters][version] = "3.2"
projects[better_exposed_filters][subdir] = "contrib"

projects[views][version] = "3.11"
projects[views][subdir] = "contrib"

projects[views_content_cache][version] = "3.0-alpha3"
projects[views_content_cache][subdir] = "contrib"

projects[views_exclude_previous][version] = "1.x-dev"
projects[views_exclude_previous][subdir] = "contrib"

projects[views_slideshow][version] = "3.1"
projects[views_slideshow][subdir] = "contrib"

projects[xmlsitemap][version] = "2.2"
projects[xmlsitemap][subdir] = "contrib"

; +++++ Libraries +++++

; CKEditor
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][type] = "library"
libraries[ckeditor][destination] = "libraries"
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.6.1/ckeditor_3.6.6.1.tar.gz"

; FitVids
libraries[fitvids][directory_name] = "fitvids"
libraries[fitvids][type] = "library"
libraries[fitvids][destination] = "libraries"
libraries[fitvids][download][type] = "get"
libraries[fitvids][download][url] = "https://raw.github.com/davatron5000/FitVids.js/master/jquery.fitvids.js"

; Flexslider
libraries[flexslider][directory_name] = "flexslider"
libraries[flexslider][type] = "library"
libraries[flexslider][destination] = "libraries"
libraries[flexslider][download][type] = "get"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider/archive/flexslider1.zip"

