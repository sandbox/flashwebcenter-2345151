<?php
/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
if (!function_exists("system_form_install_configure_form_alter")) {
  function system_form_install_configure_form_alter(&$form, $form_state) {
    $form['site_information']['site_name']['#default_value'] = 'Easy Publish';
  }
}

/**
 * Implements hook_form_alter().
 *
 * Select the current install profile by default.
 */
if (!function_exists("system_form_install_select_profile_form_alter")) {
  function system_form_install_select_profile_form_alter(&$form, $form_state) {
    foreach ($form['profile'] as $key => $element) {
      $form['profile'][$key]['#value'] = 'easy_publish';
    }
  }
}
/**
 * Implements hook_install_tasks().
 */
function easy_publish_install_tasks() {
  $tasks = array();
  $tasks['easy_publish_task_clean_up'] = array(
    'display_name' => st('Clean up'),
    'type' => 'batch',
  );
  return $tasks; 
}
/**
 * Install task callback to clean up and revert all the features then clear all caches.
 * 
 * @see easy_publish_install_tasks()
 */
function easy_publish_task_clean_up() {
  $operations = array(); 
  // Fetch the all feature information
  $features = features_get_features();
  
  // Clear the caches
  $operations[] = array('drupal_flush_all_caches', array());
  
  // Revert the features that are enabled
  foreach ($features as $name => $feature) {
    if ($feature->status) {
      $revert = array_keys($features[$name]->info['features']);
      $operations[] = array('features_revert', array(array($name => $revert)));
    }
  }
  $operations[] = array('drupal_flush_all_caches', array()); 
  // Set up private file system which defaults to the public path for now
  $operations[] = array('variable_set', array('file_private_path', variable_get('file_public_path', conf_path() . '/files'))); 
  // Rebuild node permissions
  $operations[] = array('node_access_rebuild', array()); 
  // Clear the caches
  $operations[] = array('drupal_flush_all_caches', array()); 
  // Return the batch
  return array(
    'title' => st('Cleaning up'),
    'operations' => $operations,
  );
   
}


