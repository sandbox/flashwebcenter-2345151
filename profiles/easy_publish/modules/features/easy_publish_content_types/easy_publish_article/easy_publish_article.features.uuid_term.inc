<?php
/**
 * @file
 * easy_publish_article.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function easy_publish_article_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Article Term1',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '618e79ff-0bd8-42f2-990f-6b79e3f7c1ab',
    'vocabulary_machine_name' => 'article',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Article Term2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '83d1984e-f098-4591-8332-da3256cde084',
    'vocabulary_machine_name' => 'article',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  return $terms;
}
