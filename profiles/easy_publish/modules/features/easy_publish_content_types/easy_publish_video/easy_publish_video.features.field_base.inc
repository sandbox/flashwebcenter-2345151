<?php
/**
 * @file
 * easy_publish_video.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function easy_publish_video_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_video_tags'
  $field_bases['field_video_tags'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_video_tags',
    'indexes' => array(
      'tid' => array(
        0 => 'tid',
      ),
    ),
    'locked' => 0,
    'module' => 'taxonomy',
    'settings' => array(
      'allowed_values' => array(
        0 => array(
          'vocabulary' => 'video',
          'parent' => 0,
        ),
      ),
    ),
    'translatable' => 0,
    'type' => 'taxonomy_term_reference',
  );

  return $field_bases;
}
