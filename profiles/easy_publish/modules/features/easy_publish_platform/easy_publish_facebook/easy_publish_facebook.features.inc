<?php
/**
 * @file
 * easy_publish_facebook.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function easy_publish_facebook_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
