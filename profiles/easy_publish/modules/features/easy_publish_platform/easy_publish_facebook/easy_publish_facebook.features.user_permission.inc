<?php
/**
 * @file
 * easy_publish_facebook.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function easy_publish_facebook_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer fb apps'.
  $permissions['administer fb apps'] = array(
    'name' => 'administer fb apps',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'fb',
  );

  // Exported permission: 'delete own fb_user authmap'.
  $permissions['delete own fb_user authmap'] = array(
    'name' => 'delete own fb_user authmap',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'editor' => 'editor',
    ),
    'module' => 'fb_user',
  );

  return $permissions;
}
