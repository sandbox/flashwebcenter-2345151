<?php
/**
 * @file
 * easy_publish_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function easy_publish_roles_user_default_roles() {
  $roles = array();

  // Exported role: Not Confrimed.
  $roles['Not Confrimed'] = array(
    'name' => 'Not Confrimed',
    'weight' => 1,
    'machine_name' => '',
  );

  // Exported role: editor.
  $roles['editor'] = array(
    'name' => 'editor',
    'weight' => 3,
    'machine_name' => '',
  );

  return $roles;
}
