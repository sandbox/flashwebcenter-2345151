<?php
/**
 * @file
 * easy_publish_terms.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function easy_publish_terms_taxonomy_default_vocabularies() {
  return array(
    'blog' => array(
      'name' => 'Blog',
      'machine_name' => 'blog',
      'description' => 'Taxonomy terms used to tag the blog',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'microdata' => array(
        '#attributes' => array(
          'itemscope' => '',
        ),
      ),
    ),
    'news' => array(
      'name' => 'News',
      'machine_name' => 'news',
      'description' => 'Taxonomy terms used to tage the News',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'microdata' => array(
        '#attributes' => array(
          'itemscope' => '',
        ),
      ),
    ),
    'profile' => array(
      'name' => 'Profile',
      'machine_name' => 'profile',
      'description' => 'Different types of editors in the system',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'microdata' => array(
        '#attributes' => array(
          'itemscope' => '',
        ),
      ),
    ),
    'site_pages' => array(
      'name' => 'Site Pages',
      'machine_name' => 'site_pages',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'microdata' => array(
        '#attributes' => array(
          'itemscope' => '',
        ),
      ),
    ),
    'video' => array(
      'name' => 'Video',
      'machine_name' => 'video',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'microdata' => array(
        '#attributes' => array(
          'itemscope' => '',
        ),
      ),
    ),
  );
}
