<?php
/**
 * @file
 * easy_publish_terms.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function easy_publish_terms_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer taxonomy'.
  $permissions['administer taxonomy'] = array(
    'name' => 'administer taxonomy',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in blog'.
  $permissions['delete terms in blog'] = array(
    'name' => 'delete terms in blog',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in news'.
  $permissions['delete terms in news'] = array(
    'name' => 'delete terms in news',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in profile'.
  $permissions['delete terms in profile'] = array(
    'name' => 'delete terms in profile',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in site_pages'.
  $permissions['delete terms in site_pages'] = array(
    'name' => 'delete terms in site_pages',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'delete terms in video'.
  $permissions['delete terms in video'] = array(
    'name' => 'delete terms in video',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in blog'.
  $permissions['edit terms in blog'] = array(
    'name' => 'edit terms in blog',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in news'.
  $permissions['edit terms in news'] = array(
    'name' => 'edit terms in news',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in profile'.
  $permissions['edit terms in profile'] = array(
    'name' => 'edit terms in profile',
    'roles' => array(),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in site_pages'.
  $permissions['edit terms in site_pages'] = array(
    'name' => 'edit terms in site_pages',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in video'.
  $permissions['edit terms in video'] = array(
    'name' => 'edit terms in video',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
