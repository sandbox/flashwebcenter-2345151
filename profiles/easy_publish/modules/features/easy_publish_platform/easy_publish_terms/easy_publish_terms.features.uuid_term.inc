<?php
/**
 * @file
 * easy_publish_terms.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function easy_publish_terms_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Blog Term2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '1844011c-9e3a-4d6d-b030-cbcfd96c2bbe',
    'vocabulary_machine_name' => 'blog',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Editor',
    'description' => '',
    'format' => 'plain_text',
    'weight' => 0,
    'uuid' => '406fabe9-e41c-4fd2-a405-43db53e757b4',
    'vocabulary_machine_name' => 'profile',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'SitePage Term1',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5325506e-a34e-48e7-9c34-cd5fb3240069',
    'vocabulary_machine_name' => 'site_pages',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Blog Term1',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '5450f112-eded-43f2-85be-4d4db314d862',
    'vocabulary_machine_name' => 'blog',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'News Term2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => '8790d40b-29d3-4dc7-81dc-a364f4d01c9c',
    'vocabulary_machine_name' => 'news',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'SitePage Term2',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'd687b9dc-50e3-4e32-9c38-75bd7f5e9637',
    'vocabulary_machine_name' => 'site_pages',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Assistant Editor',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'db97987d-91b3-4629-b21e-986786442b7a',
    'vocabulary_machine_name' => 'profile',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'Video1',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'e7f04065-c242-474d-a397-412e03fc8dea',
    'vocabulary_machine_name' => 'video',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  $terms[] = array(
    'name' => 'News Term1',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => 0,
    'uuid' => 'faaa766d-c401-41f2-a137-b86379a20174',
    'vocabulary_machine_name' => 'news',
    'microdata' => array(
      '#attributes' => array(
        'itemscope' => '',
      ),
    ),
  );
  return $terms;
}
