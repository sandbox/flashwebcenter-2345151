<?php
/**
 * @file
 * easy_publish_addtoany.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function easy_publish_addtoany_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer addtoany'.
  $permissions['administer addtoany'] = array(
    'name' => 'administer addtoany',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'addtoany',
  );

  return $permissions;
}
