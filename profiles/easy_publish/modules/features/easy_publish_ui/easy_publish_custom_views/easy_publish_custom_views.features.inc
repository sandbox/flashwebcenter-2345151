<?php
/**
 * @file
 * easy_publish_custom_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function easy_publish_custom_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
