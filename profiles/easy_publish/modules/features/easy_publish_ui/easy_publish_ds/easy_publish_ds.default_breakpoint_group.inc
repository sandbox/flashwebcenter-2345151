<?php
/**
 * @file
 * easy_publish_ds.default_breakpoint_group.inc
 */

/**
 * Implements hook_default_breakpoint_group().
 */
function easy_publish_ds_default_breakpoint_group() {
  $export = array();

  $breakpoint_group = new stdClass();
  $breakpoint_group->disabled = FALSE; /* Edit this to true to make a default breakpoint_group disabled initially */
  $breakpoint_group->api_version = 1;
  $breakpoint_group->machine_name = 'kingtut';
  $breakpoint_group->name = 'King Tut';
  $breakpoint_group->breakpoints = array(
    0 => 'breakpoints.theme.kingtut.desktop',
    1 => 'breakpoints.theme.kingtut.laptop',
    2 => 'breakpoints.theme.kingtut.tablet',
    3 => 'breakpoints.theme.kingtut.mobile',
  );
  $breakpoint_group->type = 'theme';
  $breakpoint_group->overridden = 0;
  $export['kingtut'] = $breakpoint_group;

  return $export;
}
