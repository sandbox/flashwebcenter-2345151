<?php
/**
 * @file
 * easy_publish_ds.default_picture_mapping.inc
 */

/**
 * Implements hook_default_picture_mapping().
 */
function easy_publish_ds_default_picture_mapping() {
  $export = array();

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Article';
  $picture_mapping->machine_name = 'article';
  $picture_mapping->breakpoint_group = 'kingtut';
  $picture_mapping->mapping = array(
    'breakpoints.theme.kingtut.desktop' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'slideshow',
      ),
    ),
    'breakpoints.theme.kingtut.laptop' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'article',
      ),
    ),
    'breakpoints.theme.kingtut.tablet' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'article',
      ),
    ),
    'breakpoints.theme.kingtut.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'article_s',
      ),
    ),
  );
  $export['article'] = $picture_mapping;

  $picture_mapping = new PictureMapping();
  $picture_mapping->disabled = FALSE; /* Edit this to true to make a default picture_mapping disabled initially */
  $picture_mapping->api_version = 2;
  $picture_mapping->label = 'Profile Photo';
  $picture_mapping->machine_name = 'profile_photo';
  $picture_mapping->breakpoint_group = 'kingtut';
  $picture_mapping->mapping = array(
    'breakpoints.theme.kingtut.desktop' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'profile_photo',
      ),
    ),
    'breakpoints.theme.kingtut.laptop' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'profile_photo',
      ),
    ),
    'breakpoints.theme.kingtut.tablet' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'profile_photo_s',
      ),
    ),
    'breakpoints.theme.kingtut.mobile' => array(
      '1x' => array(
        'mapping_type' => 'image_style',
        'image_style' => 'profile_photo_s',
      ),
    ),
  );
  $export['profile_photo'] = $picture_mapping;

  return $export;
}
