<?php
/**
 * @file
 * easy_publish_ds.ds.inc
 */

/**
 * Implements hook_ds_view_modes_info().
 */
function easy_publish_ds_ds_view_modes_info() {
  $export = array();

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'center';
  $ds_view_mode->label = 'Center';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['center'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'center_video';
  $ds_view_mode->label = 'Center Video';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['center_video'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'main_video';
  $ds_view_mode->label = 'Main Video';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['main_video'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'popular';
  $ds_view_mode->label = 'Popular';
  $ds_view_mode->entities = array(
    'node' => 'node',
  );
  $export['popular'] = $ds_view_mode;

  $ds_view_mode = new stdClass();
  $ds_view_mode->api_version = 1;
  $ds_view_mode->view_mode = 'popular_video';
  $ds_view_mode->label = 'Popular Video';
  $ds_view_mode->entities = array(
    'file' => 'file',
  );
  $export['popular_video'] = $ds_view_mode;

  return $export;
}
