<?php
/**
 * @file
 * easy_publish_ds.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function easy_publish_ds_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "breakpoints" && $api == "default_breakpoint_group") {
    return array("version" => "1");
  }
  if ($module == "breakpoints" && $api == "default_breakpoints") {
    return array("version" => "1");
  }
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "flexslider" && $api == "flexslider_default_preset") {
    return array("version" => "1");
  }
  if ($module == "flexslider_picture" && $api == "flexslider_picture_optionset") {
    return array("version" => "1");
  }
  if ($module == "picture" && $api == "default_picture_mapping") {
    return array("version" => "2");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function easy_publish_ds_image_default_styles() {
  $styles = array();

  // Exported image style: article.
  $styles['article'] = array(
    'label' => 'Article (600x350)',
    'effects' => array(
      12 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 600,
          'height' => 350,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: article_s.
  $styles['article_s'] = array(
    'label' => 'Article S (440x260)',
    'effects' => array(
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 440,
          'height' => 260,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: center.
  $styles['center'] = array(
    'label' => 'Center Thumb (405x260)',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 405,
          'height' => 260,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: most_popular.
  $styles['most_popular'] = array(
    'label' => 'Most Popular (160x100)',
    'effects' => array(
      15 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 160,
          'height' => 100,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: profile_photo.
  $styles['profile_photo'] = array(
    'label' => 'Profile Photo(280x340)',
    'effects' => array(
      16 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 280,
          'height' => 340,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: profile_photo_s.
  $styles['profile_photo_s'] = array(
    'label' => 'Profile Photo small (170x210)',
    'effects' => array(
      17 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 170,
          'height' => 210,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: slideshow.
  $styles['slideshow'] = array(
    'label' => 'Slideshow (730x430)',
    'effects' => array(
      18 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 730,
          'height' => 430,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: user_picture.
  $styles['user_picture'] = array(
    'label' => 'User Picture (88x118)',
    'effects' => array(
      19 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 88,
          'height' => 118,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
