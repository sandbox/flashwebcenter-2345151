<?php
/**
 * @file
 * easy_publish_menus.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function easy_publish_menus_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: main-menu.
  $menus['main-menu'] = array(
    'menu_name' => 'main-menu',
    'title' => 'Main menu',
    'description' => 'The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.',
  );
  // Exported menu: menu-articles-menu.
  $menus['menu-articles-menu'] = array(
    'menu_name' => 'menu-articles-menu',
    'title' => 'Articles\' Menu',
    'description' => 'The links for this menu will be all the terms you add to the vocabulary "Article"',
  );
  // Exported menu: menu-blogs-menu.
  $menus['menu-blogs-menu'] = array(
    'menu_name' => 'menu-blogs-menu',
    'title' => 'Blogs\' Menu',
    'description' => 'The links for this menu will be all the terms you add to the vocabulary "Blog"',
  );
  // Exported menu: menu-news-menu.
  $menus['menu-news-menu'] = array(
    'menu_name' => 'menu-news-menu',
    'title' => 'News\' Menu',
    'description' => 'The links for this menu will be all the terms you add to the vocabulary "News"',
  );
  // Exported menu: menu-profile-s-menu.
  $menus['menu-profile-s-menu'] = array(
    'menu_name' => 'menu-profile-s-menu',
    'title' => 'Profile\'s Menu',
    'description' => 'The links for this menu will be all the terms you add to the vocabulary "Profile"',
  );
  // Exported menu: menu-site-pages-menu.
  $menus['menu-site-pages-menu'] = array(
    'menu_name' => 'menu-site-pages-menu',
    'title' => 'Site Pages\' Menu',
    'description' => 'The links for this menu will be all the terms you add to the vocabulary "Site Pages"',
  );
  // Exported menu: menu-videos-menu.
  $menus['menu-videos-menu'] = array(
    'menu_name' => 'menu-videos-menu',
    'title' => 'Videos\' Menu',
    'description' => 'The links for this menu will be all the terms you add to the vocabulary "Video"',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Articles\' Menu');
  t('Blogs\' Menu');
  t('Main menu');
  t('News\' Menu');
  t('Profile\'s Menu');
  t('Site Pages\' Menu');
  t('The <em>Main</em> menu is used on many sites to show the major sections of the site, often in a top navigation bar.');
  t('The links for this menu will be all the terms you add to the vocabulary "Article"');
  t('The links for this menu will be all the terms you add to the vocabulary "Blog"');
  t('The links for this menu will be all the terms you add to the vocabulary "News"');
  t('The links for this menu will be all the terms you add to the vocabulary "Profile"');
  t('The links for this menu will be all the terms you add to the vocabulary "Site Pages"');
  t('The links for this menu will be all the terms you add to the vocabulary "Video"');
  t('Videos\' Menu');


  return $menus;
}
