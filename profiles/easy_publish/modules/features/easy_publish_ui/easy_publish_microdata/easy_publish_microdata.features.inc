<?php
/**
 * @file
 * easy_publish_microdata.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function easy_publish_microdata_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_microdata_mappings_defaults().
 */
function easy_publish_microdata_microdata_mappings_defaults() {
  $microdata_mappings = array();

  // Exported Microdata mapping: comment_node_article
  $microdata_mappings['comment']['comment_node_article'] = array();

  // Exported Microdata mapping: comment_node_blog
  $microdata_mappings['comment']['comment_node_blog'] = array();

  // Exported Microdata mapping: comment_node_news
  $microdata_mappings['comment']['comment_node_news'] = array();

  // Exported Microdata mapping: comment_node_page
  $microdata_mappings['comment']['comment_node_page'] = array();

  // Exported Microdata mapping: comment_node_profile
  $microdata_mappings['comment']['comment_node_profile'] = array();

  // Exported Microdata mapping: comment_node_video
  $microdata_mappings['comment']['comment_node_video'] = array();

  // Exported Microdata mapping: ds_views
  $microdata_mappings['ds_views']['ds_views'] = array();

  // Exported Microdata mapping: audio
  $microdata_mappings['file']['audio'] = array();

  // Exported Microdata mapping: document
  $microdata_mappings['file']['document'] = array();

  // Exported Microdata mapping: image
  $microdata_mappings['file']['image'] = array();

  // Exported Microdata mapping: video
  $microdata_mappings['file']['video'] = array();

  // Exported Microdata mapping: features
  $microdata_mappings['menu_link']['features'] = array();

  // Exported Microdata mapping: management
  $microdata_mappings['menu_link']['management'] = array();

  // Exported Microdata mapping: navigation
  $microdata_mappings['menu_link']['navigation'] = array();

  // Exported Microdata mapping: article
  $microdata_mappings['node']['article'] = array(
    '#itemtype' => array(
      0 => 'http://schema.org/Article',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'headline',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[node:url]',
    'field_article_tags' => array(
      '#itemprop' => array(
        0 => 'keywords',
      ),
      '#is_item' => FALSE,
    ),
    'field_image' => array(
      '#itemprop' => array(
        0 => 'image',
      ),
      '#is_item' => FALSE,
    ),
    'body' => array(
      '#itemprop' => array(
        0 => 'articleBody',
      ),
      '#is_item' => FALSE,
      'value' => array(
        '#itemprop' => array(
          0 => 'articleBody',
        ),
        '#itemtype' => '',
      ),
      'summary' => array(
        '#itemprop' => array(
          0 => 'The actual body of the article.',
        ),
        '#itemtype' => '',
      ),
    ),
  );

  // Exported Microdata mapping: blog
  $microdata_mappings['node']['blog'] = array(
    '#itemtype' => array(
      0 => 'https://schema.org/Blog',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'headline',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[node:url]',
    'field_blog_tags' => array(
      '#itemprop' => array(
        0 => 'keywords',
      ),
      '#is_item' => FALSE,
    ),
    'body' => array(
      '#itemprop' => array(
        0 => 'blogPost',
      ),
      '#is_item' => FALSE,
      'value' => array(
        '#itemprop' => array(
          0 => 'blogPost',
        ),
        '#itemtype' => '',
      ),
      'summary' => array(
        '#itemprop' => array(
          0 => 'A posting that is part of this blog. Supercedes blogPosts.',
        ),
        '#itemtype' => '',
      ),
    ),
  );

  // Exported Microdata mapping: news
  $microdata_mappings['node']['news'] = array(
    '#itemtype' => array(
      0 => 'https://schema.org/NewsArticle',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'headline',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[node:url]',
    'field_news_tags' => array(
      '#itemprop' => array(
        0 => 'keywords',
      ),
      '#is_item' => FALSE,
    ),
    'field_image' => array(
      '#itemprop' => array(
        0 => 'image',
      ),
      '#is_item' => FALSE,
    ),
    'body' => array(
      '#itemprop' => array(
        0 => 'articleBody',
      ),
      '#is_item' => FALSE,
      'value' => array(
        '#itemprop' => array(
          0 => 'articleBody',
        ),
        '#itemtype' => '',
      ),
      'summary' => array(
        '#itemprop' => array(
          0 => 'The actual body of the article.',
        ),
        '#itemtype' => '',
      ),
    ),
  );

  // Exported Microdata mapping: page
  $microdata_mappings['node']['page'] = array(
    '#itemtype' => array(
      0 => 'http://schema.org/WebPage',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'headline',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[node:url]',
    'field_page_tags' => array(
      '#itemprop' => array(
        0 => 'keywords',
      ),
      '#is_item' => FALSE,
    ),
    'body' => array(
      '#itemprop' => array(
        0 => 'text',
      ),
      '#is_item' => FALSE,
      'value' => array(
        '#itemprop' => array(
          0 => 'text',
        ),
        '#itemtype' => '',
      ),
      'summary' => array(
        '#itemprop' => array(
          0 => 'The text body of the site page',
        ),
        '#itemtype' => '',
      ),
    ),
    'field_image' => array(
      '#itemprop' => array(
        0 => 'image',
      ),
      '#is_item' => FALSE,
    ),
  );

  // Exported Microdata mapping: profile
  $microdata_mappings['node']['profile'] = array(
    '#itemtype' => array(
      0 => 'http://schema.org/ProfilePage',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'name',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[node:url]',
    'field_first_name' => array(
      '#itemprop' => array(
        0 => 'name',
      ),
      '#is_item' => FALSE,
    ),
    'field_last_name' => array(
      '#itemprop' => array(
        0 => 'name',
      ),
      '#is_item' => FALSE,
    ),
    'field_job_title' => array(
      '#itemprop' => array(
        0 => 'genre',
      ),
      '#is_item' => FALSE,
    ),
    'field_email' => array(
      '#itemprop' => array(
        0 => 'name',
      ),
      '#is_item' => FALSE,
    ),
    'field_image' => array(
      '#itemprop' => array(
        0 => 'primaryImageOfPage',
      ),
      '#is_item' => FALSE,
    ),
    'body' => array(
      '#itemprop' => array(
        0 => 'description',
      ),
      '#is_item' => FALSE,
      'value' => array(
        '#itemprop' => array(
          0 => 'description',
        ),
        '#itemtype' => '',
      ),
      'summary' => array(
        '#itemprop' => array(
          0 => 'A short biography about the person.',
        ),
        '#itemtype' => '',
      ),
    ),
    'field_profile_tags' => array(
      '#itemprop' => array(
        0 => 'keywords',
      ),
      '#is_item' => FALSE,
    ),
    'field_facebook_page' => array(
      '#itemprop' => array(
        0 => 'url',
      ),
      '#is_item' => FALSE,
    ),
    'field_twitter_username' => array(
      '#itemprop' => array(
        0 => 'url',
      ),
      '#is_item' => FALSE,
    ),
    'field_phone_number' => array(
      '#itemprop' => array(
        0 => 'tel',
      ),
      '#is_item' => FALSE,
    ),
  );

  // Exported Microdata mapping: video
  $microdata_mappings['node']['video'] = array(
    '#itemtype' => array(
      0 => 'http://schema.org/VideoObject',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'headline',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[node:url]',
    'field_video_tags' => array(
      '#itemprop' => array(
        0 => 'keywords',
      ),
      '#is_item' => FALSE,
    ),
    'body' => array(
      '#itemprop' => array(
        0 => 'description',
      ),
      '#is_item' => FALSE,
      'value' => array(
        '#itemprop' => array(
          0 => 'description',
        ),
        '#itemtype' => '',
      ),
      'summary' => array(
        '#itemprop' => array(
          0 => 'some description about the video.',
        ),
        '#itemtype' => '',
      ),
    ),
    'field_image' => array(
      '#itemprop' => array(
        0 => 'video',
      ),
      '#is_item' => FALSE,
    ),
  );

  // Exported Microdata mapping: rules_config
  $microdata_mappings['rules_config']['rules_config'] = array();

  // Exported Microdata mapping: search_api_index
  $microdata_mappings['search_api_index']['search_api_index'] = array();

  // Exported Microdata mapping: search_api_page
  $microdata_mappings['search_api_page']['search_api_page'] = array();

  // Exported Microdata mapping: search_api_server
  $microdata_mappings['search_api_server']['search_api_server'] = array();

  // Exported Microdata mapping: article
  $microdata_mappings['taxonomy_term']['article'] = array();

  // Exported Microdata mapping: blog
  $microdata_mappings['taxonomy_term']['blog'] = array();

  // Exported Microdata mapping: news
  $microdata_mappings['taxonomy_term']['news'] = array();

  // Exported Microdata mapping: profile
  $microdata_mappings['taxonomy_term']['profile'] = array();

  // Exported Microdata mapping: site_pages
  $microdata_mappings['taxonomy_term']['site_pages'] = array();

  // Exported Microdata mapping: video
  $microdata_mappings['taxonomy_term']['video'] = array();

  // Exported Microdata mapping: taxonomy_vocabulary
  $microdata_mappings['taxonomy_vocabulary']['taxonomy_vocabulary'] = array();

  // Exported Microdata mapping: user
  $microdata_mappings['user']['user'] = array(
    '#itemtype' => array(
      0 => 'http://schema.org/Person',
    ),
    'title' => array(
      '#itemprop' => array(
        0 => 'name',
      ),
    ),
    '#is_item' => TRUE,
    '#itemid_token' => '[user:url]',
    'field_first_name' => array(
      '#itemprop' => array(
        0 => 'name',
      ),
      '#is_item' => FALSE,
    ),
    'field_last_name' => array(
      '#itemprop' => array(
        0 => 'anme',
      ),
      '#is_item' => FALSE,
    ),
  );

  // Exported Microdata mapping: wysiwyg_profile
  $microdata_mappings['wysiwyg_profile']['wysiwyg_profile'] = array();

  return $microdata_mappings;
}
