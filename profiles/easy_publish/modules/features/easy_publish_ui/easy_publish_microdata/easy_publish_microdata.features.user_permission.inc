<?php
/**
 * @file
 * easy_publish_microdata.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function easy_publish_microdata_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer microdata'.
  $permissions['administer microdata'] = array(
    'name' => 'administer microdata',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'microdata',
  );

  return $permissions;
}
