<?php
/**
 * @file
 * easy_publish_microdata.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function easy_publish_microdata_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_schema_installed';
  $strongarm->value = TRUE;
  $export['metatag_schema_installed'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_article';
  $strongarm->value = array(
    0 => 'node',
  );
  $export['microdata_article'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_blog';
  $strongarm->value = array(
    0 => 'node',
  );
  $export['microdata_blog'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_enabled_vocabularies';
  $strongarm->value = array(
    'schema_org' => 'schema_org',
  );
  $export['microdata_enabled_vocabularies'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_listing';
  $strongarm->value = array(
    0 => 'node',
  );
  $export['microdata_listing'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_news';
  $strongarm->value = array(
    0 => 'node',
  );
  $export['microdata_news'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_page';
  $strongarm->value = array(
    0 => 'node',
  );
  $export['microdata_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_profile';
  $strongarm->value = array(
    0 => 'node',
  );
  $export['microdata_profile'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'microdata_video';
  $strongarm->value = array(
    0 => 'node',
  );
  $export['microdata_video'] = $strongarm;

  return $export;
}
