<?php
/**
 * @file
 * easy_publish_text_format.features.filter.inc
 */

/**
 * Implements hook_filter_default_formats().
 */
function easy_publish_text_format_filter_default_formats() {
  $formats = array();

  // Exported format: Filtered HTML.
  $formats['filtered_html'] = array(
    'format' => 'filtered_html',
    'name' => 'Filtered HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => 0,
    'filters' => array(
      'filter_html' => array(
        'weight' => -49,
        'status' => 1,
        'settings' => array(
          'allowed_html' => '<a> <address> <em> <strong> <b> <i> <big> <small> <sub> <sup> <cite> <code> <img> <ul> <ol> <li> <dl> <lh> <dt> <dd> <br> <p> <table> <th> <td> <tr> <pre> <blockquote> <h1> <h2> <h3> <h4> <h5> <h6> <hr>',
          'filter_html_help' => 1,
          'filter_html_nofollow' => 0,
        ),
      ),
      'smart_paging_filter' => array(
        'weight' => -46,
        'status' => 1,
        'settings' => array(),
      ),
      'twitter_username' => array(
        'weight' => -45,
        'status' => 1,
        'settings' => array(),
      ),
      'twitter_links' => array(
        'weight' => -44,
        'status' => 1,
        'settings' => array(),
      ),
      'twitter_hashtag' => array(
        'weight' => -43,
        'status' => 1,
        'settings' => array(),
      ),
      'smart_paging_filter_autop' => array(
        'weight' => -41,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => -40,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -37,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => -36,
        'status' => 1,
        'settings' => array(
          'settings_source' => 'global',
          'local_paths' => '',
          'protocol_style' => 'full',
        ),
      ),
    ),
  );

  // Exported format: Full HTML.
  $formats['full_html'] = array(
    'format' => 'full_html',
    'name' => 'Full HTML',
    'cache' => 1,
    'status' => 1,
    'weight' => 1,
    'filters' => array(
      'smart_paging_filter' => array(
        'weight' => -48,
        'status' => 1,
        'settings' => array(),
      ),
      'twitter_hashtag' => array(
        'weight' => -47,
        'status' => 1,
        'settings' => array(),
      ),
      'twitter_username' => array(
        'weight' => -46,
        'status' => 1,
        'settings' => array(),
      ),
      'twitter_links' => array(
        'weight' => -45,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => -42,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'smart_paging_filter_autop' => array(
        'weight' => -41,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_htmlcorrector' => array(
        'weight' => -37,
        'status' => 1,
        'settings' => array(),
      ),
      'pathologic' => array(
        'weight' => -36,
        'status' => 1,
        'settings' => array(
          'settings_source' => 'global',
          'local_paths' => '',
          'protocol_style' => 'full',
          'local_settings' => array(
            'protocol_style' => 'full',
            'local_paths' => '',
          ),
        ),
      ),
      'filter_autop' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'picture' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'twitter_embed' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'media_filter' => array(
        'weight' => 2,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  // Exported format: Plain text.
  $formats['plain_text'] = array(
    'format' => 'plain_text',
    'name' => 'Plain text',
    'cache' => 1,
    'status' => 1,
    'weight' => 10,
    'filters' => array(
      'filter_html_escape' => array(
        'weight' => 0,
        'status' => 1,
        'settings' => array(),
      ),
      'filter_url' => array(
        'weight' => 1,
        'status' => 1,
        'settings' => array(
          'filter_url_length' => 72,
        ),
      ),
      'filter_autop' => array(
        'weight' => 2,
        'status' => 1,
        'settings' => array(),
      ),
    ),
  );

  return $formats;
}
