<?php

/**
 * @file
 * Extended microdata integration for Link module.
 */

/**
 * Implements hook_microdata_suggestions().
 */
function link_microdata_suggestions() {
  $suggestions = array();

  $suggestions['fields']['link_field']['schema.org'] = array(
    '#itemprop' => array('webPage'),
    '#is_item' => TRUE,
    '#itemtype' => array('http://schema.org/WebPage'),
    'url' => array(
      '#itemprop' => array('url'),
    ),
    'title' => array(
      '#itemprop' => array('name'),
    ),
  );

  return $suggestions;
}
