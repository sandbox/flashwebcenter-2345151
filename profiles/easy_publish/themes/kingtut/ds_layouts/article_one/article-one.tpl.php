<?php

/**
 * @file
 * Display Suite 1 column template.
 */
?>
<div class="kingtut-one article-one" <?php print $layout_attributes;?>>
  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <?php if ($contentzone): ?>
      <?php print $contentzone; ?>
  <?php endif; ?>
</div>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>