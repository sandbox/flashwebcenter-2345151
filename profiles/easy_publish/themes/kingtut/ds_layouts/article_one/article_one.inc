<?php
function ds_article_one() {
  return array(
    'label' => t('Article One'),
    'regions' => array(
      'contentzone' => t('Content')
    ),
    // Add this line if there is a default css file.
    'css' => FALSE,
  );
}
?>
