<?php
function ds_blog_one() {
  return array(
    'label' => t('Blog One'),
    'regions' => array(
      'contentzone' => t('Content')
    ),
    // Add this line if there is a default css file.
    'css' => FALSE,
  );
}
?>
