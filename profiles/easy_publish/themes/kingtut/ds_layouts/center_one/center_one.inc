<?php
function ds_center_one() {
  return array(
    'label' => t('Center One'),
    'regions' => array(
      'contentzone' => t('Content')
    ),
    // Add this line if there is a default css file.
    'css' => FALSE,
  );
}
?>
