<?php
function ds_center_term() {
  return array(
    'label' => t('Center Term'),
    'regions' => array(
      'contentzone' => t('Content')
    ),
    // Add this line if there is a default css file.
    'css' => FALSE,
  );
}
?>
