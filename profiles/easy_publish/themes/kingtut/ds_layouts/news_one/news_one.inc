<?php
function ds_news_one() {
  return array(
    'label' => t('News One'),
    'regions' => array(
      'contentzone' => t('Content')
    ),
    // Add this line if there is a default css file.
    'css' => FALSE,
  );
}
?>
