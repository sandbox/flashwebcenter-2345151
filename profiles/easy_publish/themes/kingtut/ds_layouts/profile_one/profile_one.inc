<?php
function ds_profile_one() {
  return array(
    'label' => t('Profile One'),
    'regions' => array(
      'contentzone' => t('Content')
    ),
    // Add this line if there is a default css file.
    'css' => FALSE,
  );
}
?>
