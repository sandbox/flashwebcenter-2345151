jQuery(document).ready(function() {
  //Hide the filter description and and hide the description on registration form
  jQuery("#edit-body .fieldset-wrapper,#edit-comment-body .fieldset-wrapper, #edit-body-format .fieldset-wrapper").prepend('<h3 class="posting-guides">Posting Guidlines</h3>');
  jQuery("h3.posting-guides").click(function() {
    jQuery(".filter-guidelines").slideToggle("fast");
  });
  //Add .trigger to registration form													
  jQuery("form#user-register-form #edit-mail, form#user-register-form #edit-conf-mail, form#user-register-form #edit-name,  form#user-register-form #edit-captcha-response, form#user-register-form #edit-pass-pass1, form#user-register-form .password-suggestions, form#user-login #edit-name, form#user-login #edit-pass, form#user-login #edit-captcha-response, form#user-pass #edit-name").addClass('trigger');
  //Just for request new password 
  jQuery("form#user-pass #edit-captcha-response").focus(function() {
    jQuery(".form-item-captcha-response .description").slideToggle("fast");
  });
  jQuery("form#user-register-form #edit-pass-pass2").focus(function() {
    jQuery(".form-item-pass .password-suggestions").slideUp("slow");
  });
  //on focus expand the description 								
  jQuery(".trigger").focus(function() {
    var currentTrigger = jQuery(this);
    currentTrigger.addClass('current');
    jQuery('.trigger:not(.current)').next(".description").slideUp('slow', function() {
      currentTrigger.removeClass('current').next(".description").fadeIn('fast');
    });
  });
  // edit user account page			
  jQuery("#edit-timezone, #edit-privatemsg, #edit-heartbeat, #edit-contact").addClass("collapsed");
  // For the quick tab
  jQuery(".tab_content").slideUp("slow");
  jQuery(".tab_content:first").slideDown("slow");;
  jQuery("ul.listing-tabs li").click(function() {
    jQuery("ul.listing-tabs li").removeClass("active");
    jQuery(this).addClass("active");
    jQuery(".tab_content").slideUp("slow");
    var activeTab = jQuery(this).attr("rel");
    jQuery("#" + activeTab).slideDown("slow");
  });
  // remove any br from views paragraph									
  jQuery(".views-row p br, #footer > div.section > br").remove();
  // move the password strength after the input
  jQuery(".form-item-pass-pass1 .password-strength").insertAfter(".form-item-pass-pass1 #edit-pass-pass1");
  // Making sure the height are equal in both column	
  var facetLeft = jQuery(".left-web .video-one .watch-video").height();
  var facetRight = jQuery(".left-web .video-one .center-image").height();
  	      if (facetLeft > facetRight){ jQuery(".left-web .video-one .center-image").height(facetLeft)}
           else{ jQuery(".left-web .video-one .watch-video").height(facetRight)};
  // Making sure the height are equal in both column	
  var facetLeft = jQuery(".right-web .video-one .watch-video").height();
  var facetRight = jQuery(".right-web .video-one .popular-image").height();
  	      if (facetLeft > facetRight){ jQuery(".right-web .video-one .popular-image").height(facetLeft)}
           else{ jQuery(".right-web .video-one .watch-video").height(facetRight)}; 
          
             
  
});

	
jQuery("document").ready(function($){
    
    var nav = $('.flexmenu');
    var headerHeight = jQuery("#header-wrapper").height();

    $(window).scroll(function () {
        if ($(this).scrollTop() > headerHeight) {
            nav.addClass("f-nav");
        } else {
            nav.removeClass("f-nav");
        }
    });
 
});
   