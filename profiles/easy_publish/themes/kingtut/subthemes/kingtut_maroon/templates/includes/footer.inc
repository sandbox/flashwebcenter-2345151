<?php $block = module_invoke('system', 'block_view', 'main-menu');
      print render($block['content']);
     ?>
<div class="copyright">
	Copyright &copy; <?php print(date('Y') . ' ');?> - All Rights Reserved - <a
		title="Drupal Care" href="http://www.DrupalCare.com/">Drupal Care</a>
	|| Design &amp; Hosted by <a title="Austin Drupal Web Design"
		href="http://www.flashwebcenter.com/">Flash Web Center</a>
</div>
<?php print render($page['footer']);?>