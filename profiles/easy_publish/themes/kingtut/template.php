<?php
/**
 * Add body classes if certain regions have content.
 */
/* field--machine-name.tpl
print_r(array_keys($content));
print render($content['taxanomy_tags']); */

function kingtut_css_alter(&$css) {
	$exclude = array(
		'misc/vertical-tabs.css' => FALSE,
		'modules/aggregator/aggregator.css' => FALSE,
		'modules/block/block.css' => FALSE,
		'modules/book/book.css' => FALSE,
		'modules/comment/comment.css' => FALSE,
		'modules/dblog/dblog.css' => FALSE,
		'modules/file/file.css' => FALSE,
		'modules/field/theme/field.css' => FALSE,
		'modules/filter/filter.css' => FALSE,
		'modules/help/help.css' => FALSE,
		'modules/menu/menu.css' => FALSE,
		'modules/node/node.css' => FALSE,
		'modules/openid/openid.css' => FALSE,
		'modules/poll/poll.css' => FALSE,
		'modules/profile/profile.css' => FALSE,
		'modules/search/search.css' => FALSE,
		'modules/statistics/statistics.css' => FALSE,
		'modules/syslog/syslog.css' => FALSE,
		'modules/system/admin.css' => FALSE,
		'modules/system/maintenance.css' => FALSE,
		'modules/system/system.css' => FALSE,
		'modules/system/system.admin.css' => FALSE,
		'modules/system/system.base.css' => FALSE,
		'modules/system/system.maintenance.css' => FALSE,
		'modules/system/system.menus.css' => FALSE,
		'modules/system/system.messages.css' => FALSE,
		'modules/system/system.theme.css' => FALSE,
		'modules/taxonomy/taxonomy.css' => FALSE,
		'modules/tracker/tracker.css' => FALSE,
		'modules/update/update.css' => FALSE,
		'modules/user/user.css' => FALSE,
		'sites/all/modules/contrib/ctools/css/ctools.css' => FALSE,
		'sites/all/modules/contrib/views/css/views.css' => FALSE,
		'sites/all/modules/contrib/logintoboggan/logintoboggan.css' => FALSE,
		'misc/ui/jquery.ui.theme.css' => FALSE,
		'misc/ui/jquery.ui.core.css' => FALSE,
		'sites/all/modules/contrib/admin_menu/admin_menu.uid1.css' => FALSE,
	);
	$css = array_diff_key($css, $exclude);
}

/**
 * setting the maintenance page
 */
function kingtut_preprocess_maintenance_page(&$vars) {
	if (!$vars['db_is_active']) {
		unset($vars['site_name']);
	}
	drupal_add_css(drupal_get_path('theme', 'kingtut') . '/css/maintenance-page.css');
}

/**
 * Implements hook_html_head_alter().
 * This will overwrite the default meta character type tag with HTML5 version.
 */
function kingtut_html_head_alter(&$head_elements) {
	$head_elements['system_meta_content_type']['#attributes'] = array('charset' => 'utf-8');
}

/**
 * <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
 */
function kingtut_page_alter($page) {
	$viewport = array(
		'#type' => 'html_tag',
		'#tag' => 'meta',
		'#attributes' => array(
			'name' => 'viewport',
			'content' => 'width=device-width, initial-scale=1, maximum-scale=1',
		),
	);
	drupal_add_html_head($viewport, 'viewport');
}

/**
 * Changes the search form to use the HTML5 "search" input attribute
 */
function kingtut_preprocess_search_block_form(&$vars) {
	$vars['search_form'] = str_replace('type="text"', 'type="search"', $vars['search_form']);
}

/**
 * Remove height and width from drupal images.
 */
function kingtut_preprocess_image(&$vars) {
	foreach (array('width', 'height') as $key) {
		if (isset($vars[$key])) {
			unset($vars[$key]);
		}
		if (isset($vars['attributes'][$key])) {
			unset($vars['attributes'][$key]);
		}
	}
}

/**
 * Override or insert vars into the block template.
 */
function kingtut_preprocess_block(&$vars) {
	// In the header region visually hide block titles.
	if ($vars['block']->region == 'header') {
		$vars['title_attributes_array']['class'][] = 'element-invisible';
	}
}

/**
 * Implements theme_menu_tree().
 */
function kingtut_menu_tree($vars) {
	return '<ul class="menu clearfix">' . $vars['tree'] . '</ul>';
}

/**
 * Implements theme_field__field_type().
 */
function kingtut_field__taxonomy_term_reference($vars) {
	$output = '';

	// Render the label, if it's not hidden.
	if (!$vars['label_hidden']) {
		$output .= '<h3 class="field-label">' . $vars['label'] . ': </h3>';
	}

	// Render the items.
	$output .= ($vars['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
	foreach ($vars['items'] as $delta => $item) {
		$output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $vars['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
	}
	$output .= '</ul>';

	// Render the top-level DIV.
	$output = '<div class="' . $vars['classes'] . (!in_array('clearfix', $vars['classes_array']) ? ' clearfix' : '') . '"' . $vars['attributes'] . '>' . $output . '</div>';

	return $output;
}

/**
 * Override some node to be printed in a specific node type.
 */
function kingtut_preprocess_node(&$vars) {
	if ($vars['view_mode'] == 'full' && node_is_page($vars['node'])) {
		$vars['classes_array'][] = 'node-full';
	}
}

function kingtut_preprocess_page(&$vars) {
	// setting the views page to use different pages
	if (($views_page = views_get_page_view()) && $views_page->name == "center") {
		$vars['theme_hook_suggestions'][] = 'page__' . 'center';
	} elseif (($views_page = views_get_page_view()) && $views_page->name == "related") {
		$vars['theme_hook_suggestions'][] = 'page__' . 'center';
	} elseif (($views_page = views_get_page_view()) && $views_page->name == "most_popular") {
		$vars['theme_hook_suggestions'][] = 'page__' . 'center';
	}

	// setting the default page for most user pages
	global $user;
	$path = drupal_get_path_alias();
	if ($path == 'search') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'user') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'user/password') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'user/register') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'contact') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'users/' . $user->uid) {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'user/' . $user->uid . '/edit') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'user/' . $user->uid . '/track') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'user/' . $user->uid . '/edit/twitter') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'search';
	} elseif ($path == 'profile') {
		$vars['theme_hook_suggestions'][] = 'page__' . 'profile';
	}

	// setting the default page for all the vocabulary
	// (If you add a new vocabulary, please add another switch case with the $vid).
	if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
		$tid = arg(2);
		$vid = db_query("SELECT vid FROM {taxonomy_term_data} WHERE tid = :tid", array(':tid' => $tid))->fetchField();
		switch ($vid) {
			case "2":
				// To view the vocuabulary link (blog)
				$vars['theme_hook_suggestions'][] = 'page__' . 'center';
				break;
			case "3":
				// To view the vocuabulary link (Article)
				$vars['theme_hook_suggestions'][] = 'page__' . 'center';
				break;
			case "4":
				// To view the vocuabulary link (News)
				$vars['theme_hook_suggestions'][] = 'page__' . 'center';
				break;
			case "5":
				// To view the vocuabulary link (Site Pages)
				$vars['theme_hook_suggestions'][] = 'page__' . 'center';
				break;
			case "6":
				// To view the vocuabulary link (Video)
				$vars['theme_hook_suggestions'][] = 'page__' . 'center';
				break;
			case "7":
				// To view the vocuabulary link (Profile)
				$vars['theme_hook_suggestions'][] = 'page__' . 'profile';
				break;
			default:
				$vars['theme_hook_suggestions'][] = 'page__' . 'center';
		}
	}

	// setting the default page for all content types
	if (isset($vars['node'])) {
		switch ($vars['node']) {
			case $vars['node']->type == "article":
				$vars['theme_hook_suggestions'][] = 'page__' . 'article';
				break;
			case $vars['node']->type == "blog":
				$vars['theme_hook_suggestions'][] = 'page__' . 'article';
				break;
			case $vars['node']->type == "page":
				$vars['theme_hook_suggestions'][] = 'page__' . 'article';
				break;
			case $vars['node']->type == "news":
				$vars['theme_hook_suggestions'][] = 'page__' . 'article';
				break;
			case $vars['node']->type == "video":
				$vars['theme_hook_suggestions'][] = 'page__' . 'article';
				break;
			case $vars['node']->type == "profile":
				$vars['theme_hook_suggestions'][] = 'page__' . 'profile';
				break;
			default:
				$vars['theme_hook_suggestions'][] = 'page__';
		}
	}

	// setting some text for the user.
	if (arg(0) == 'user') {
		switch (arg(1)) {
			case 'register':
				drupal_set_title(t('Create New Account'));
				break;
			case 'password':
				drupal_set_title(t('Retrieve New Password'));
				break;
			case '':
			case 'login':
				drupal_set_title(t('User Login'));
				break;
		}
	}
}

/**
 * Override some of the user forms.
 */
function kingtut_theme($existing, $type, $theme, $path) {
	return array(
		'user_register' => array(
			'render element' => 'form',
			'template' => 'templates/user/user-register',
		),
		'user_login_block' => array(
			'render element' => 'form',
			'template' => 'templates/user/user-login-block',
		),
		'user_login' => array(
			'render element' => 'form',
			'template' => 'templates/user/user-login',
		),
		'user_pass' => array(
			'render element' => 'form',
			'template' => 'templates/user/user-pass',
		),
		'contact_site_form' => array(
			'render element' => 'form',
			'template' => 'templates/user/contact-site-form',
		),
	);
}

/**
 * Add red Strike with the required field in the form
 */
function kingtut_form_required_marker($vars) {
	// This is also used in the installer, pre-database setup.
	$t = get_t();
	$attributes = array(
		'class' => 'form-required',
		'title' => $t('This field cannot be left blank.'),
	);
	return '<span class="red bold"' . drupal_attributes($attributes) . '>*</span>';
}

/**
 * Remove the comment filters' tips
 */
//function kingtut_filter_tips($tips, $long = FALSE, $extra = '') {  return '';}
/**
 * Remove the comment filter's more information tips link
 */
function kingtut_filter_tips_more_info() {
	return '';
}

/**
 * Change the text on readmore to whatever you want
 */
function kingtut_node_view_alter(&$build) {
	//$build['links']['node']['#links']['node-readmore']['title'] =t('');
	//$build['links']['node']['#links']['comment-add']['title'] =t('');
	//$build['links']['node']['#links']['blog_usernames_blog']['title'] =t('');
	//unset($build['links']['comment']['#links']['comment_forbidden']);
	//unset($build['links']['comment']['#links']['blog_sitenames_blog']);
}
