<div id="main-menu-wrapper" class="flexmenu">
    <nav id="main-menu" role="navigation">
        <?php $block = module_invoke('system', 'block_view', 'main-menu');
      print render($block['content']);
        ?>
    </nav>
</div>
<!-- #main-menu -->