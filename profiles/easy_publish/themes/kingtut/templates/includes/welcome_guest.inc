<div id="user-menu">
    <?php global $user; ?>
    <?php if ($logged_in): ?>
    <span class="welc-guest">Welcome <?php print l($user -> name, 'user/' . $user -> uid); ?> - </span>
    <span class="not-loged">Not <?php print l($user -> name, 'user/logout'); ?>?</span>
    <?php else: ?>
    <span class="guest"><?php print l("Register ", "user/register"); ?>-
        <?php print l(" Login ", "user"); ?>Or Login Using</span>
    <div class="sign-in-twitter">
        <?php $block = module_invoke('twitter_signin', 'block_view', '0'); ?>
        <?php print render($block['content']); ?>
        <?php $block = module_invoke('fb_connect', 'block_view', 'login_facebook_login'); ?>
        <?php print render($block['content']); ?>
    </div>
    <?php endif; ?>
</div>