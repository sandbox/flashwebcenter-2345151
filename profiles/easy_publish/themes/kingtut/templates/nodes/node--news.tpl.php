<article id="node-<?php print $node -> nid; ?>"
class="<?php print $classes; ?> main-news clearfix" <?php print $attributes; ?>>
    <header>
        <?php print render($title_prefix); ?>
        <?php // print_r(array_keys($content)); ?>
        <h1 class="title"<?php print $title_attributes; ?>><?php print $title; ?>
        <?php print render($content['title_microdata']); ?></h1>
        <?php print render($title_suffix); ?>
        <div class ="article-info">
            <span class="author" itemprop="author">Created By: <?php print $name; ?></span>
            <br />
            <span class="author" itemprop="date-created">Date Created: <?php print date("l F j, Y", $node -> created); ?></span>
            <span class="author" itemprop="date-updated">Last Updated: <?php
                  if (isset($node -> changed)) {
                    print date("l F j, Y", $node -> changed);
                  }
                ?></span>
        </div>
        <div class="social-links">
            <?php print render($content['print_links']); ?>
            <?php print render($content['addtoany']); ?>
        </div>
    </header>
    <div class="content" <?php print $content_attributes; ?>>
        <?php print render($content['field_image']); ?>
        <?php print render($content['body']); ?>
    </div>
    <!-- /.content -->
    <footer>
        <?php print render($content['field_news_tags']); ?>
        <?php print render($content['links']); ?>
        <?php print render($content['flippy_pager']); ?>
    </footer>
    <?php print render($content['comments']); ?>
</article>
<!-- /.node -->
