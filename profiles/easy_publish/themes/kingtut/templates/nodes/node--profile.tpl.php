<article id="node-<?php print $node -> nid; ?>"class="<?php print $classes; ?> main-profile clearfix" <?php print $attributes; ?>>
    <header>
        <?php print render($title_prefix); ?>
        <?php // print_r(array_keys($content)); ?>
        <?php  //print '<pre>' . print_r($variables,1) . '</pre>'; ?>
        <h1 class="title" <?php print $title_attributes; ?>><?php print $title; ?>
        <?php print render($content['title_microdata']); ?></h1>
        <?php print render($title_suffix); ?>
    </header>
    <div class="content" <?php print $content_attributes; ?>>
        <div class="profile-top">
            <div class="profile-top-left">
                <?php print render($content['field_image']); ?>
            </div>
            <div class="profile-top-right">
                <?php print render($content['field_first_name']); ?>
                <?php print render($content['field_last_name']); ?>
                <?php print render($content['field_job_title']); ?>
                <?php print render($content['field_email']); ?>
                <?php print render($content['field_phone_number']); ?>
                <?php print render($content['field_twitter_username']); ?>
                <?php print render($content['field_facebook_page']); ?>
                <?php print render($content['field_profile_tags']); ?>
                <div class="social-links">
                    <?php print render($content['print_links']); ?>
                    <?php print render($content['addtoany']); ?>
                </div>
            </div>
        </div>
        <?php print render($content['body']); ?>
    </div>
    <!-- /.content -->
    <footer>
        <?php print render($content['links']); ?>
        <?php print render($content['flippy_pager']); ?>
    </footer>
</article>
<!-- /.node -->
