<article id="node-<?php print $node -> nid; ?>" class="<?php print $classes; ?> main-default clearfix"<?php print $attributes; ?>>
  <?php if ($user_picture || !$page || $display_submitted): ?>
  <header> <?php print $user_picture; ?> <?php print render($title_prefix); ?>
    <?php if (!$page): ?>
    <h2 itemprop="headline"<?php print $title_attributes; ?>><?php print $title; ?></h2>
    <?php endif; ?>
    <?php print render($title_suffix); ?>
    <?php if ($display_submitted): ?>
    <p class="submitted" itemprop="author">
      <?php
      print t('Submitted by !username on !datetime', array('!username' => $name,'!datetime' => $date));
          ?>
    </p>
    <?php endif; ?>
  </header>
  <?php endif; ?>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
    // We hide the comments, tags and links now so that we can render them later.
    hide($content['comments']);
    hide($content['links']);
    hide($content['field_tags']);
    print render($content);
    ?>
  </div>
  <!-- /.content --> 
  <?php if (!empty($content['field_tags']) || !empty($content['links'])): ?>
  <footer> <?php print render($content['field_tags']); ?> <?php print render($content['links']); ?> </footer>
  <?php endif; ?>
  <?php print render($content['comments']); ?> </article>
<!-- /.node --> 
