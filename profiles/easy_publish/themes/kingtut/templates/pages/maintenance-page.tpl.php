<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language -> language; ?>" lang="<?php print $language -> language; ?>" dir="<?php print $language -> dir; ?>">
<head>
<?php print $head; ?>
<title><?php print $head_title; ?></title>
<?php print $styles; ?><?php print $scripts; ?>
</head>
<body class="<?php print $classes . " maintenance-page"; ?>" <?php print $attributes; ?>>
<div id="skip-link"> <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a> </div>
<div id="page-wrapper">
  <div id="header-wrapper">
    <header id="header">
      <div class="section clearfix">
        <?php if ($site_name):?>
        <h1 id="site-name"> <a href="<?php print $front_page; ?>" title="<?php print $site_name; ?>"
		rel="home"> <?php print $site_name; ?> </a> </h1>
        <?php endif; ?>
        <?php if ($site_slogan):?>
        <h2 id="site-slogan"> <a href="<?php print $front_page; ?>"
		title="<?php print $site_slogan; ?>" rel="home"> <?php print $site_slogan; ?> </a> </h2>
        <?php endif; ?>
      </div>
    </header>
  </div>
  <!-- /header-wrapper || /header || /section  || /clearfix -->
  <div id="main-wrapper">
    <div id="main" class="default-page clearfix">
      <div class="left-web">
        <?php if ($messages):?>
        <div id="messages"> <?php print $messages; ?> </div>
        <?php endif; ?>
        <?php print render($title_prefix); ?>
        <?php if ($title): ?>
        <h1 class="title" id="page-title"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?> <?php print $content; ?> 
        </div>      
      <!-- /.section, /#sidebar-second -->
      <div class="clear"></div>
    </div>
  </div>
  <!-- /main-wrapper || /main || /section || /clearfix--> 
  
</div>
<!--/page || Page-Wrapper////////-->
</body>
</html>
