<div id="page-wrapper">
    <div id="header-wrapper">
        <header id="header">
            <div class="section clearfix">
                <?php
                include kingtut_theme_base_get_themed_file("/templates/includes/header.inc");
                ?>
            </div>
        </header>
    </div>
    <?php
    include kingtut_theme_base_get_themed_file("/templates/includes/main_menu.inc");
    ?>
    <!-- /header-wrapper || /header || /section  || /clearfix -->
    <div id="main-wrapper">
        <div id="main" class="home-page clearfix">
            <div class="left-web">
                 <?php if ($messages):
                 ?>
                 <div id="messages">
                 <?php print $messages; ?>
                 </div>
                <?php endif; ?>
                <?php
                  include kingtut_theme_base_get_themed_file("/templates/includes/homepage_content.inc");
               ?>
            </div>
            <aside class="right-web" role="complementary">
                <div class="section">
                    <?php
                    include kingtut_theme_base_get_themed_file("/templates/includes/welcome_guest.inc");
                    include kingtut_theme_base_get_themed_file("/templates/includes/most_popular.inc");
                    ?>
                </div>
            </aside>
            <!-- /.section, /#sidebar-second -->
            <div class="clear"></div>
        </div>
    </div>
    <!-- /main-wrapper || /main || /section || /clearfix-->
    <div id="footer-wrapper" class="clearfix">
        <footer id="footer" role="contentinfo">
            <div class="section clearfix">
                <?php
                include kingtut_theme_base_get_themed_file("/templates/includes/footer.inc");
                ?>
            </div>
        </footer>
    </div>
    <!-- /footer-wrapper|| /footer || /section -->
</div>
<!--/page || Page-Wrapper////////-->
