<div id="page-wrapper">
    <div id="header-wrapper">
        <header id="header">
            <div class="section clearfix">
                <?php
                include    kingtut_theme_base_get_themed_file("/templates/includes/header.inc");
                ?>
            </div>
        </header>
    </div>
    <?php
    include    kingtut_theme_base_get_themed_file("/templates/includes/main_menu.inc");
    ?>
    <!-- /header-wrapper || /header || /section  || /clearfix -->
    <div id="main-wrapper">
        <div id="main" class="profile-page clearfix">
            <div class="left-web">
                <?php if ($messages):
                ?>
                <div id="messages">
                    <?php print $messages; ?>
                </div>
                <?php endif; ?>
                <?php if ($tabs):
                ?>
                <div class="tabs">
                    <?php print render($tabs); ?>
                </div>
                <?php endif; ?>
                <?php print render($page['content']); ?>
            </div>
            <aside class="right-web" role="complementary">
                <div class="section">
                    <?php
                    include    kingtut_theme_base_get_themed_file("/templates/includes/welcome_guest.inc");
                    include    kingtut_theme_base_get_themed_file("/templates/includes/authors_related_articles.inc");
                    include    kingtut_theme_base_get_themed_file("/templates/includes/most_popular_profiles.inc");
                    ?>
                </div>
            </aside>
            <!-- /.section, /#sidebar-second -->
            <div class="clear"></div>
        </div>
    </div>
    <!-- /main-wrapper || /main || /section || /clearfix-->
    <div id="footer-wrapper" class="clearfix">
        <footer id="footer" role="contentinfo">
            <div class="section clearfix">

            </div>
        </footer>
    </div>
    <!-- /footer-wrapper|| /footer || /section -->
</div>
<!--/page || Page-Wrapper////////-->