<?php if ($search_results) : ?>
  <h1><?php print t('Search results'); ?></h1>
  <ul class="search-results <?php print $module; ?>-results">
    <?php print $search_results; ?>
  </ul>
  <?php print $pager; ?>
<?php else : ?>
  <h4><?php print t('There are no results for that search'); ?></h4>
  <div class="description clearfix"><?php print search_help('search#noresults', drupal_help_arg()); ?></div>
<?php endif; ?>



