<article class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <header>
    <?php if ($new): ?>
    <span class="new"><?php print $new ?></span>
    <?php endif; ?>
    <?php print render($title_prefix); ?>
    <h2 class="comment-title"<?php print $title_attributes; ?>><?php print $title ?></h2>
    <?php print render($title_suffix); ?> </header>
  <div class="content"<?php print $content_attributes; ?>>
    <?php
    // We hide the links now so that we can render them later.
    hide($content['links']);
    print render($content);
    ?>
    <?php if ($signature): ?>
    <div class="user-signature clearfix"> <?php print $signature ?> </div>
    <!-- /.user-signature -->
    <?php endif; ?>
  </div>
  <!-- /.content -->
  
  <?php if (!empty($content['links'])): ?>
  <footer>
    <div class ="comment-info"> <?php print $permalink; ?> <span class="author" itemprop="author">Posted By: <?php print $author; ?> </span> <span class="posted" itemprop="date-created">Posted on: <?php print date("l F j, Y", $comment -> created); ?> </span> <?php print render($content['links']) ?> </div>
  </footer>
  <?php endif; ?>
</article>
<!-- /.comment --> 
