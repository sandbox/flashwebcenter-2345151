<?php
// var_dump($form);
// print_r(array_keys($form));
print drupal_render($form['form_build_id']);
print drupal_render($form['form_id']);
print drupal_render($form['form_token']);
print drupal_render($form['metatags']);
print drupal_render($form['name']);
print drupal_render($form['pass']);
print drupal_render($form['captcha']);
print drupal_render($form['actions']['submit']);
?>
<hr class="clear"  />
<div class="login-social">
    <h1 class="page-title title"> OR LOG IN BY USING A  SOCIAL  CHANEL </h1>
    <?php 
     $block = module_invoke('twitter_signin', 'block_view', '0'); 
     print render($block['content']); 
     $block = module_invoke('fb_connect', 'block_view', 'login_facebook_login'); 
     print render($block['content']); 
     ?>
</div>
<hr class="clear"  />
<div class="login-tabs">
    <a href="/user/password">Request New Password</a> - <a href="/user/register">Create New Account</a>
</div>

