<?php // print_r(array_keys($form));
print drupal_render($form['form_build_id']);
print drupal_render($form['form_id']);
print drupal_render($form['form_token']);
print drupal_render($form['metatags']);
?>
<h1> Get started – it's free.</h1>
<p>Registration takes less than 2 minutes.</p>
<hr class="clear"  />
<?php
print drupal_render($form['field_first_name']);
// prints the custom first name field
print drupal_render($form['field_last_name']);
// prints the custom last name field
print drupal_render($form['account']['name']);
// prints the username field
print drupal_render($form['account']['mail']);
// prints the mail field
print drupal_render($form['account']['conf_mail']);
// prints the mail field
print drupal_render($form['account']['pass']);
// prints the password fields
print drupal_render($form['captcha']);
// prints the Captcha
print drupal_render($form['terms_of_use']);
// prints the Captcha
print drupal_render($form['actions']['submit']);
// print the submit button
 ?>
<br />
<div class="login-tabs">
    <a href="/user/login">Log In</a> - <a href="/user/password">Request New Password</a>
</div>