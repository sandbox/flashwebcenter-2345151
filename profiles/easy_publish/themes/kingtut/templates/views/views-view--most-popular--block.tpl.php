<div class="<?php print $classes; ?>">
  <?php
  print render($title_prefix);
  if ($title) : print $title;
  endif;
  print render($title_suffix);
  if ($rows) : print $rows;
  endif; 
  ?>
</div>
